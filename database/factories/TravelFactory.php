<?php

use Faker\Generator as Faker;

$factory->define(StrappTravels\Models\Travel::class, function (Faker $faker) {
    return [
        'code' => $faker->randomNumber,
    	'price' => $faker->randomFloat(2, 1, 100),
    	'places' => 100,
    	'currency' => 'USD',
    	'departure_at' => $faker->dateTime,
    	'origen_id' => factory(StrappTravels\Models\Place::class)->create()->id,
    	'destiny_id' => factory(StrappTravels\Models\Place::class)->create()->id
    ];
});
