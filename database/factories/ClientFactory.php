<?php

use Faker\Generator as Faker;

$factory->define(StrappTravels\Models\Client::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
    	'last_name' => $faker->lastName,
    	'id_number' => $faker->numberBetween(10, 100) * 100000,
    	'address' => $faker->address,
    	'phone' => '1234567890'
    ];
});
