<?php

use Faker\Generator as Faker;

$factory->define(StrappTravels\Models\Place::class, function (Faker $faker) {
    return [
        'place' => $faker->country . ', ' . $faker->city
    ];
});
