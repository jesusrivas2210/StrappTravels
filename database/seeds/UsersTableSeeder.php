<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(StrappTravels\User::class)->create([
        	'email' => 'travel@correo.com'
        ]);
    }
}
