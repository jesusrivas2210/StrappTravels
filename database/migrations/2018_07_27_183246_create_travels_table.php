<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travels', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('code')->comment('Codigo del viaje')->unique();
            $table->unsignedInteger('places')->comment('Plazas totales del viaje');
            $table->float('price', 8, 2)->comment('Precio del viaje');
            $table->enum('currency', ['USD', 'EU'])->comment('Tipo de moneda');
            $table->dateTime('departure_at')->comment('Fecha y hora de salida');
            $table->unsignedInteger('origen_id')->comment('Lugar de origen');
            $table->unsignedInteger('destiny_id')->comment('Lugar de destino');
            $table->timestamps();

            $table->foreign('origen_id')
                    ->references('id')
                    ->on('places')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreign('destiny_id')
                    ->references('id')
                    ->on('places')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travels');
    }
}
