<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTravelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_travel', function (Blueprint $table) {
            $table->unsignedInteger('client_id')->comment('Cliente');
            $table->unsignedInteger('travel_id')->comment('Viaje');

            $table->primary(['client_id', 'travel_id']);
            $table->foreign('client_id')
                    ->references('id')
                    ->on('clients')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                    
            $table->foreign('travel_id')
                    ->references('id')
                    ->on('travels')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_travel');
    }
}
