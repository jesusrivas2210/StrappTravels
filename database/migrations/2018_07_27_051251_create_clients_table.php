<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->comment('Nombre del cliente');
            $table->string('last_name')->comment('Apellido del cliente');
            $table->string('id_number')->unique()->comment('Cedula del cliente')->unique();
            $table->string('address')->comment('Direccion del cliente');
            $table->string('phone')->comment('Telefono del cliente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
