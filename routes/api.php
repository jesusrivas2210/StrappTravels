<?php

Route::resource('clients', 'ClientController', ['except' => ['create', 'edit']]);

Route::resource('travels', 'TravelController', ['except' => ['create', 'edit']]);

Route::resource('travels/{travel}/clients', 'TravelClientController', ['except' => ['create', 'edit']]);

Route::get('clients/{client}/travels', 'TravelClientController@clientTravels');

Route::resource('places', 'PlaceController', ['except' => ['create', 'edit']]);