<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Place;

class UpdatePlaceTest extends TestCase
{
    protected $place;

    public function setUp(){
        parent::setUp();
        $this->place = factory(Place::class)->create();
    }
    
    /**
     * @test
     */
    public function an_authenticated_user_can_update_places()
    {
        // given => teniendo un usuario autenticado, un lugar en la base de datos y los
        // datos para actualizar al lugar
        $this->setActingAs();
        $payload = [
            'place' => 'Venezuela Caracas', 
        ];

        // when => cuando se hace patch request a /api/places/{place}
        $response = $this->json('PATCH', $this->url(), $payload);

        // then => entonces de retorna un status code 201 y el nuevo lugar creado.
        //         Y se ve el nuevo registro en la base de datos
        $response->assertStatus(200)
                ->assertJson(
                    [
                        'data' => $payload
                    ]   
                );
        $table = 'places';
        $this->assertDatabaseHas($table, $payload);
    }

    /**
     * @test
     */
    public function an_user_cannot_update_places()
    {
        // given => teniendo un usuario NO autenticadoun, un lugar en la base de datos y los
        // datos para actualizar al lugar
        $payload = [
            'place' => 'Venezuela Caracas', 
        ];

        // when => cuando se hace post request a /api/places
        $response = $this->json('PATCH', $this->url(), $payload);

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_places_with_invalid_first_name()
    {
        $this->withExceptionHandling();
        // given => teniendo un usuario autenticado y el nombre del lugar es no valido
        $this->setActingAs();
        $field = 'place';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 100;
        $this->inputValidation($value, $field, 'PATCH');

        $value = str_random(101);
        $this->inputValidation($value, $field, 'PATCH');
    }

    

   

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_places_with_same_name()
    {
        $this->setActingAs();
        $field = 'place';
        $otherPlace = factory(Place::class)->create();
        $this->inputValidation($otherPlace->place, $field, 'PATCH');
    }

    

    
    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_places_that_not_exists()
    {
        // given => teniendo un usuario autenticado y un lugar en la base de datos
        $this->setActingAs();
        $payload = [
            'place' => 'Caracas'
        ];

        // when => cuando se hace un patch request a la url con un lugar inexistente
        $response = $this->json('PATCH', 'api/places/1000', $payload);

        // then => entonces de retorna un status code 404 NOT FOUND
        $response->assertStatus(404);
    }

    public function url() {
        return 'api/places/' . $this->place->id;
    }
}
