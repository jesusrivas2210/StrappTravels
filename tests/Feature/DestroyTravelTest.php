<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Place;
use StrappTravels\Models\Travel;

class DestroyTravelTest extends TestCase
{
    protected $travel;

    public function setUp(){
        parent::setUp();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $this->travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);
    }


    /**
     * @test
     */
    public function an_authenticated_user_can_delete_travels()
    {
        // given => teniendo un usuario autenticado y un viaje en la base de datos
        $this->setActingAs();

        // when => cuando se hace delete request a /api/travels/{travel}
        $response = $this->json('DELETE', $this->url());

        // then => entonces de retorna un status code 200 
        $response->assertStatus(200)
                ->assertJson(
                    [
                        'deleted' => true
                    ]   
                );
        $table = 'travels';
        $this->assertDatabaseMissing($table, ['id' => $this->travel->id]);
    }

    /**
     * @test
     */
    public function an_user_cannot_delete_travels()
    {
        // given => teniendo un viaje en la base de datos 
        
        // when => cuando se hace delete request a /api/travels/{travel}
        $response = $this->json('DELETE', $this->url());

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_delete_travels_that_not_exists()
    {
        // given => teniendo un usuario autenticado y un viaje en la base de datos
        $this->setActingAs();

        // when => cuando se hace un delete request a la url con un viaje inexistente
        $response = $this->json('delete', 'api/travels/1000');

        // then => entonces de retorna un status code 404 NOT FOUND
        $response->assertStatus(404);
    }

    public function url() {
        return 'api/travels/' . $this->travel->id;
    }
}
