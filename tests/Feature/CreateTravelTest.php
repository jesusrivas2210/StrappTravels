<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Place;

class CreateTravelTest extends TestCase
{
    /**
     * @test
     */
    public function an_authenticated_user_can_create_travels()
    {
         
        // given => teniendo un usuario autenticado y los datos del viaje
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $payload = [
            'code' => '123456789',
            'price' => '100000',
            'places' => '50',
            'currency' => 'USD',
            'departure_at' => '2020-01-01 10:00:00',
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id,         
        ];

        // when => cuando se hace post request a /api/travels
        $response = $this->json('POST', $this->url(), $payload);

        // then => entonces de retorna un status code 201
        //         Y se ve el nuevo registro en la base de datos
        $response->assertStatus(201);
        $table = 'travels';
        $this->assertDatabaseHas($table, $payload);
    }

    /**
     * @test
     */
    public function an_user_cannot_update_travels()
    {
        // given => teniendo un usuario NO autenticado y los datos del viaje
        $payload = [
            'code' => '123456789',
            'price' => 1000.00,
            'places' => 50,
            'currency' => 'USD', 
        ];

        // when => cuando se hace post request a /api/travels
        $response = $this->json('POST', $this->url(), $payload);

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_travels_with_invalid_code()
    {
        // given => teniendo un usuario autenticado y el codigo es no valido
        $this->setActingAs();
        $field = 'code';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 'codigo';
        $this->inputValidation($value, $field);

    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_travels_with_same_code()
    {
        // given => teniendo un usuario autenticado y los datos del lugar
        $this->setActingAs();
        $orien = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $payload = [
            'code' => '123456789',
            'price' => '1000.00',
            'places' => 50,
            'currency' => 'USD',
            'departure_at' => now(),
            'origen_id' => $orien->id,
            'destiny_id' => $destiny->id,         
        ];

        // when => cuando se hacen dos post request a /api/travels
        $this->json('POST', $this->url(), $payload);

        $response = $this->json('POST', $this->url(), $payload);

        // then => entonces de retorna un status code 422 Unprocessable Entity
        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_travels_with_invalid_price()
    {
        // given => teniendo un usuario autenticado y el precio es no valido
        $this->setActingAs();
        $field = 'price';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 'precio';
        $this->inputValidation($value, $field);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_travels_with_invalid_places()
    {
        // given => teniendo un usuario autenticado y los lugares no son validos
        $this->setActingAs();
        $field = 'places';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 'lugares';
        $this->inputValidation($value, $field);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_travels_with_invalid_currency()
    {
        // given => teniendo un usuario autenticado y la moneda no es valida
        $this->setActingAs();
        $field = 'currency';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 'moneda';
        $this->inputValidation($value, $field);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_travels_with_invalid_departure_date()
    {
        // given => teniendo un usuario autenticado y la fecha de salida no es valida
        $this->setActingAs();
        $field = 'departure_at';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 'fecha';
        $this->inputValidation($value, $field);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_travels_with_invalid_origen()
    {
        // given => teniendo un usuario autenticado y el origen no es valido
        $this->setActingAs();
        $field = 'origen_id';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 'origen';
        $this->inputValidation($value, $field);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_travels_with_invalid_destiny()
    {
        // given => teniendo un usuario autenticado y el destino no es valido
        $this->setActingAs();
        $field = 'destiny_id';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 'destino';
        $this->inputValidation($value, $field);
    }

    /**
     * @test
     */
    public function origin_and_destiny_must_be_diferents()
    {
        // given => teniendo un usuario autenticado y el origen y el destino iguales
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $payload = [
            'code' => '123456789',
            'price' => '100000',
            'places' => '50',
            'currency' => 'USD',
            'departure_at' => '2020-01-01 10:00:00',
            'origen_id' => $origen->id,
            'destiny_id' => $origen->id,         
        ];

        // when => cuando se hace post request a /api/travels
        $response = $this->json('POST', $this->url(), $payload);


        // then => entonces de retorna un status code 422 Unprocessable Entity
        $response->assertStatus(422);
    }


    public function url() {
        return 'api/travels';
    }
}
