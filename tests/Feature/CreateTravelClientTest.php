<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Place;
use StrappTravels\Models\Client;
use StrappTravels\Models\Travel;

class CreateTravelClientTest extends TestCase
{
    /**
     * @test
     */
    public function an_authenticated_user_can_attach_clients_to_travel()
    {
        // given => teniendo un usuario autenticado y los datos del cliente y el viaje
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);
        $client = factory(Client::class)->create();
        $payload = [
            'client_id' => $client->id
        ];

        // when => cuando se hace post request a /api/travels/{travel}/clients
        $response = $this->json('POST', $this->url() . '/' . $travel->id . '/clients', 
            $payload);

        // then => entonces de retorna un status code 201.
        //         Y se ve el nuevo registro en la base de datos
        $response->assertStatus(201)
                ->assertJson(
                    [
                        'attached' => true
                    ]   
                );
        $table = 'client_travel';
        $this->assertDatabaseHas($table, ['client_id' => $client->id, 'travel_id' => $travel->id]);

    }

    /**
     * @test
     */
    public function an_user_cannot_attach_clients_to_travel()
    {
        // given => teniendo un usuario NO autenticado y los datos del cliente y el viaje
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);
        $client = factory(Client::class)->create();
        $payload = [
            'client_id' => $client->id
        ];

        // when => cuando se hace post request a /api/travels/{travel}/clients
        $response = $this->json('POST', $this->url() . '/' . $travel->id . '/clients', 
            $payload);

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }


    /**
     * @test
     */
    public function an_authenticated_user_cannot_attach_clients_to_travel_with_invalid_client_id()
    {
        // given => teniendo un usuario autenticado y el cliente es no valido
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);

        $payload = [
            'client_id' => ''
        ];

        // when => cuando se hace un post  request a la url con data incorrecta
        $response = $this->json('post', $this->url()  . '/' . $travel->id . '/clients', $payload);

        // then => entonces de retorna un status code 422 Unprocessable Entity
        $response->assertStatus(422)
                ->assertJsonValidationErrors('client_id');

        $payload = [
            'client_id' => 'cliente'
        ];

        // when => cuando se hace un post  request a la url con data incorrecta
        $response = $this->json('post', $this->url()  . '/' . $travel->id . '/clients', $payload);

        // then => entonces de retorna un status code 422 Unprocessable Entity
        $response->assertStatus(422)
                ->assertJsonValidationErrors('client_id');

        
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_attach_clients_to_travel_when_client_not_exists()
    {
        // given => teniendo un usuario autenticado y el cliente es no valido
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);

        $payload = [
            'client_id' => 50
        ];

        // when => cuando se hace un post  request a la url con data incorrecta
        $response = $this->json('post', $this->url()  . '/' . $travel->id . '/clients', $payload);

        // then => entonces de retorna un status code 422 Unprocessable Entity
        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_attach_clients_when_there_is_not_place_in_travel()
    {
        // given => teniendo un usuario autenticado y los datos del cliente y el viaje
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id,
            'places' => 0
        ]);
        $client = factory(Client::class)->create();
        $payload = [
            'client_id' => $client->id
        ];

        // when => cuando se hace post request a /api/travels/{travel}/clients
        $response = $this->json('POST', $this->url() . '/' . $travel->id . '/clients', 
            $payload);

        // then => entonces de retorna un status code 400.
        //         Y se ve el nuevo registro en la base de datos
        $response->assertStatus(400);
        $table = 'client_travel';
        $this->assertDatabaseMissing($table, ['client_id' => $client->id, 'travel_id' => $travel->id]);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_attach_same_client_twice_to_the_same_travel()
    {
        // given => teniendo un usuario autenticado y los datos del cliente y el viaje
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);
        $client = factory(Client::class)->create();
        $travel->clients()->attach($client->id);
        $payload = [
            'client_id' => $client->id
        ];

        // when => cuando se hace post request a /api/travels/{travel}/clients
        $response = $this->json('POST', $this->url() . '/' . $travel->id . '/clients', 
            $payload);

        // then => entonces de retorna un status code 400.
        //         Y se ve el nuevo registro en la base de datos
        $response->assertStatus(400);

    }

    public function url() {
        return 'api/travels';
    }
}
