<?php

namespace Tests\Feature;

use Tests\TestCase;

class CreatePlaceTest extends TestCase
{
    /**
     * @test
     */
    public function an_authenticated_user_can_create_places()
    {
        // given => teniendo un usuario autenticado y los datos del lugar
        $this->setActingAs();
        $payload = [
        	'place' => 'Venezuela Caracas',     	
        ];

        // when => cuando se hace post request a /api/places
        $response = $this->json('POST', $this->url(), $payload);

        // then => entonces de retorna un status code 201 y el nuevo lugar creado.
        //         Y se ve el nuevo registro en la base de datos
        $response->assertStatus(201)
        		->assertJson(
        			[
        				'data' => $payload
        			]	
        		);
        $table = 'places';
        $this->assertDatabaseHas($table, $payload);
    }

    /**
     * @test
     */
    public function an_user_cannot_update_places()
    {
    	// given => teniendo un usuario NO autenticado y los datos del lugar
        $payload = [
        	'place' => 'Venezuela, Caracas', 
        ];

        // when => cuando se hace post request a /api/places
        $response = $this->json('POST', $this->url(), $payload);

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_places_with_invalid_place()
    {
    	// given => teniendo un usuario autenticado y el pais es no valido
        $this->setActingAs();
        $field = 'place';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 100;
        $this->inputValidation($value, $field);

        $value = str_random(101);
        $this->inputValidation($value, $field);
    }

    

    

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_places_with_same_country_and_city()
    {
        // given => teniendo un usuario autenticado y los datos del lugar
        $this->setActingAs();
        $payload = [
            'place' => 'Caracas, Venezuela'
        ];

        // when => cuando se hacen dos post request a /api/places
        $this->json('POST', $this->url(), $payload);

        $response = $this->json('POST', $this->url(), $payload);

        // then => entonces de retorna un status code 422 Unprocessable Entity
        $response->assertStatus(422);
    }


    public function url() {
        return 'api/places';
    }
}
