<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Place;

class DestroyPlaceTest extends TestCase
{
    protected $place;

    public function setUp(){
        parent::setUp();
        $this->place = factory(Place::class)->create();
    }


    /**
     * @test
     */
    public function an_authenticated_user_can_delete_places()
    {
        // given => teniendo un usuario autenticado y un lugar en la base de datos
        $this->setActingAs();

        // when => cuando se hace delete request a /api/places/{place}
        $response = $this->json('DELETE', $this->url());

        // then => entonces de retorna un status code 200 
        $response->assertStatus(200)
                ->assertJson(
                    [
                        'deleted' => true
                    ]   
                );
        $table = 'places';
        $this->assertDatabaseMissing($table, ['id' => $this->place->id]);
    }

    /**
     * @test
     */
    public function an_user_cannot_delete_places()
    {
        // given => teniendo un lugar en la base de datos 
        
        // when => cuando se hace delete request a /api/places/{place}
        $response = $this->json('DELETE', $this->url());

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_delete_places_that_not_exists()
    {
        // given => teniendo un usuario autenticado y un lugar en la base de datos
        $this->setActingAs();

        // when => cuando se hace un delete request a la url con un lugar inexistente
        $response = $this->json('delete', 'api/places/1000');

        // then => entonces de retorna un status code 404 NOT FOUND
        $response->assertStatus(404);
    }

    public function url() {
        return 'api/places/' . $this->place->id;
    }
}
