<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Place;
use StrappTravels\Models\Travel;

class UpdateTravelTest extends TestCase
{
    protected $travel;

    public function setUp(){
        parent::setUp();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $this->travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);
    }
    
    /**
     * @test
     */
    public function an_authenticated_user_can_update_travels()
    {
        // given => teniendo un usuario autenticado, un viaje en la base de datos y los
        // datos para actualizar al viaje
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $payload = [
            'code' => '123456789',
            'price' => '100000',
            'places' => '50',
            'currency' => 'USD',
            'departure_at' => '2020-01-01 10:00:00',
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ];

        // when => cuando se hace patch request a /api/travels/{travel}
        $response = $this->json('PATCH', $this->url(), $payload);

        // then => entonces de retorna un status code 201 y el nuevo viaje creado.
        //         Y se ve el nuevo registro en la base de datos
        $response->assertStatus(200)
                ->assertJson(
                    [
                        'data' => [
                            'code' => '123456789',
                            'price' => '100000',
                            'places' => '50',
                            'currency' => 'USD',
                            'departure_at' => '2020-01-01 10:00:00',
                        ]
                    ]   
                );
        $table = 'travels';
        $this->assertDatabaseHas($table, $payload);
    }

    /**
     * @test
     */
    public function an_user_cannot_update_travels()
    {
        // given => teniendo un usuario NO autenticadoun, un viaje en la base de datos y los
        // datos para actualizar al viaje
        $payload = [
            'code' => '123456789',
            'price' => '100000',
            'places' => '50',
            'currency' => 'USD',
            'departure_at' => '2020-01-01 10:00:00',
        ];

        // when => cuando se hace post request a /api/travels
        $response = $this->json('PATCH', $this->url(), $payload);

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_travels_with_invalid_code()
    {
        $this->withExceptionHandling();
        // given => teniendo un usuario autenticado y el codigo del viaje es no valido
        $this->setActingAs();
        $field = 'code';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 'codigo';
        $this->inputValidation($value, $field, 'PATCH');

    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_travels_with_same_code()
    {
        $this->setActingAs();
        $field = 'code';
        $origen = factory(Place::class)->create();
        $otherTravel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $origen->id
        ]);
        $this->inputValidation($otherTravel->code, $field, 'PATCH');
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_travels_with_invalid_price()
    {
        // given => teniendo un usuario autenticado y el precio del viaje no valido
        $this->setActingAs();
        $field = 'price';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 'precio';
        $this->inputValidation($value, $field, 'PATCH');
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_travels_with_invalid_places()
    {
        // given => teniendo un usuario autenticado y los lugares del viaje no validos
        $this->setActingAs();
        $field = 'places';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 'lugar';
        $this->inputValidation($value, $field, 'PATCH');

        
    }

    

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_travels_with_invalid_currency()
    {
        $this->setActingAs();
        $field = 'currency';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 100;
        $this->inputValidation($value, $field, 'PATCH');

        $value = 'moneda';
        $this->inputValidation($value, $field, 'PATCH');
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_travels_with_invalid_departure_date()
    {
        // given => teniendo un usuario autenticado y la fecha del viaje no valida
        $this->setActingAs();
        $field = 'departure_at';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 'fecha';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 123456789;
        $this->inputValidation($value, $field, 'PATCH');

    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_travels_with_invalid_origen()
    {
        // given => teniendo un usuario autenticado y el origen no es valido
        $this->setActingAs();
        $field = 'origen_id';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 'origen';
        $this->inputValidation($value, $field, 'PATCH');
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_travels_with_invalid_destiny()
    {
        // given => teniendo un usuario autenticado y el destino no es valido
        $this->setActingAs();
        $field = 'destiny_id';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 'destino';
        $this->inputValidation($value, $field, 'PATCH');
    }

    /**
     * @test
     */
    public function origin_and_destiny_must_be_diferents_on_update()
    {
        // given => teniendo un usuario autenticado y el origen y el destino iguales
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $payload = [
            'code' => '123456789',
            'price' => '100000',
            'places' => '50',
            'currency' => 'USD',
            'departure_at' => '2020-01-01 10:00:00',
            'origen_id' => $origen->id,
            'destiny_id' => $origen->id,         
        ];

        // when => cuando se hace post request a /api/travels
        $response = $this->json('PATCH', $this->url(), $payload);


        // then => entonces de retorna un status code 422 Unprocessable Entity
        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_travels_that_not_exists()
    {
        // given => teniendo un usuario autenticado y un viaje en la base de datos
        $this->setActingAs();
        $payload = [
            'price' => '1234567890'
        ];

        // when => cuando se hace un patch request a la url con un viaje inexistente
        $response = $this->json('PATCH', 'api/travels/1000', $payload);

        // then => entonces de retorna un status code 404 NOT FOUND
        $response->assertStatus(404);
    }

    public function url() {
        return 'api/travels/' . $this->travel->id;
    }
}
