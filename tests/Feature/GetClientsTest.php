<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Client;

class GetClientsTest extends TestCase
{
	protected $clients;

	public function setUp(){
        parent::setUp();
        $this->clients = factory(Client::class)->create();
    }

    /**
     * @test
     */
    public function an_authenticated_user_can_get_all_clients()
    {
        // given => teniendo un usuario autenticado y varios clientes en la base de datos
        $this->setActingAs();

        // when => cuando se hace get request a /api/clients
        $response = $this->json('get', $this->url());

        // then => entonces de retorna un status code 200 y la lista de clientes
        $response->assertStatus(200)->assertJson(
        	[
        		'data' => [
        			[
        				'id' => $this->clients->id,
        				'first_name' => $this->clients->first_name,
        				'last_name' => $this->clients->last_name,
        				'id_number' => $this->clients->id_number,
        				'address' => $this->clients->address,
        				'phone' => $this->clients->phone,
        				'created_at' => $this->clients->created_at,
        				'updated_at' => $this->clients->updated_at,
        			]
	        	] 
	        ]
        );
    }

    /**
     * @test
     */
    public function an_user_cannot_get_clients()
    {
    	// given => teniendo varios clientes en la base de datos y un usuario no autenticado
        // when => cuando se hace get request a /api/clients
        $response = $this->json('get', $this->url());

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_can_get_client_by_id()
    {
    	// given => teniendo un usuario autenticado y varios clientes en la base de datos
        $this->setActingAs();

        // when => cuando se hace get request a /api/clients/{client}
        $response = $this->json('get', $this->url() . '/' . $this->clients->id);

        // then => entonces de retorna un status code 200 y el cliente del id
        $response->assertStatus(200)->assertJson(
        	[
        		'data' => [
        			
    				'id' => $this->clients->id,
    				'first_name' => $this->clients->first_name,
    				'last_name' => $this->clients->last_name,
    				'id_number' => $this->clients->id_number,
    				'address' => $this->clients->address,
    				'phone' => $this->clients->phone,
    				'created_at' => $this->clients->created_at,
    				'updated_at' => $this->clients->updated_at,
        			
	        	] 
	        ]
        );	
	
	}

    /**
     * @test
     */
    public function an_user_cannot_get_client_by_id()
    {
    	// given => teniendo varios clientes en la base de datos y un usuario no autenticado

        // when => cuando se hace get request a /api/clients/{client}
        $response = $this->json('get', $this->url() . '/' . $this->clients->id);

         // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_get_client_by_id_that_not_exists()
    {
    	// given => teniendo un usuario autenticado y varios clientes en la base de datos
        $this->setActingAs();

        // when => cuando se hace get request a /api/clients/{client}
        $response = $this->json('get', $this->url() . '/1000');

        // then => entonces de retorna un status code 404 not found
        $response->assertStatus(404);	
	
	}


    public function url() {
        return 'api/clients';
    }
}
