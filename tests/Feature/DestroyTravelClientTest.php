<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Place;
use StrappTravels\Models\Client;
use StrappTravels\Models\Travel;

class DestroyTravelClientTest extends TestCase
{
    /**
     * @test
     */
    public function an_authenticated_user_can_detach_clients_from_travel()
    {
        // given => teniendo un usuario autenticado y los datos del cliente y el viaje
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);
        $client = factory(Client::class)->create();
        $travel->clients()->attach($client->id);


        // when => cuando se hace delete request a /api/travels/{travel}/clients/{client}
        $response = $this->json('DELETE', '/api/travels/' . $travel->id . '/clients/' . $client->id);

        // then => entonces de retorna un status code 200 
        $response->assertStatus(200)
                ->assertJson(
                    [
                        'deleted' => true
                    ]   
                );
        $table = 'client_travel';
        $this->assertDatabaseMissing($table, ['travel_id' => $travel->id, 'client_id' => $client->id]);
    }

    /**
     * @test
     */
    public function an_user_cannot_detach_clients_from_travel()
    {
        // when => cuando se hace delete request a /api/travels/{travel}/clients/{client}
        $response = $this->json('DELETE', '/api/travels/1/clients/2');

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    public function url() {
        return '';
    }
}
