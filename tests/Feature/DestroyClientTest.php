<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Client;

class DestroyClientTest extends TestCase
{

	protected $client;

    public function setUp(){
        parent::setUp();
        $this->client = factory(Client::class)->create();
    }


    /**
     * @test
     */
    public function an_authenticated_user_can_delete_clients()
    {
    	// given => teniendo un usuario autenticado y un cliente en la base de datos
        $this->setActingAs();

        // when => cuando se hace delete request a /api/clients/{client}
        $response = $this->json('DELETE', $this->url());

        // then => entonces de retorna un status code 200 
        $response->assertStatus(200)
                ->assertJson(
                    [
                        'deleted' => true
                    ]   
                );
        $table = 'clients';
        $this->assertDatabaseMissing($table, ['id' => $this->client->id]);
    }

    /**
     * @test
     */
    public function an_user_cannot_delete_clients()
    {
        // given => teniendo un cliente en la base de datos 
       	
        // when => cuando se hace delete request a /api/clients/{client}
        $response = $this->json('DELETE', $this->url());

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_delete_clients_that_not_exists()
    {
        // given => teniendo un usuario autenticado y un cliente en la base de datos
        $this->setActingAs();

        // when => cuando se hace un delete request a la url con un cliente inexistente
        $response = $this->json('delete', 'api/clients/1000');

        // then => entonces de retorna un status code 404 NOT FOUND
        $response->assertStatus(404);
    }

    public function url() {
        return 'api/clients/' . $this->client->id;
    }
}
