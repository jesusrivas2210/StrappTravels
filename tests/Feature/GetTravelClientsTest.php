<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Place;
use StrappTravels\Models\Client;
use StrappTravels\Models\Travel;

class GetTravelClientsTest extends TestCase
{
    /**
     * @test
     */
    public function an_authenticated_user_can_get_travel_with_clients()
    {
        // given => teniendo un usuario autenticado y los datos del cliente y el viaje
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);
        $client = factory(Client::class)->create();
        $travel->clients()->attach($client->id);

        // when => cuando se hace get request a /api/travels/{travel}/clients
        $response = $this->json('get', '/api/travels/' . $travel->id . '/clients');

        // then => entonces de retorna un status code 200 y la lista de clientes
        $response->assertStatus(200)
            ->assertJson(
            [
                'data' => [
                    
                    'id' => $travel->id,
                    'code' => $travel->code,
                    'price' => $travel->price,
                    'places' => $travel->places,
                    'currency' => $travel->currency,
                   
                    'clients' => [
                        [
                            'id' => $client->id
                        ]
                    ]
                    
                ] 
            ]
        );
    }

    /**
     * @test
     */
    public function an_authenticated_user_can_get_client_with_travels()
    {
         // given => teniendo un usuario autenticado y los datos del cliente y el viaje
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);
        $client = factory(Client::class)->create();
        $travel->clients()->attach($client->id);

        // when => cuando se hace get request a /api/clients/{client}/travels
        $response = $this->json('get', '/api/clients/' . $client->id . '/travels');

        // then => entonces de retorna un status code 200
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function an_authenticated_user_can_get_travel_with_client()
    {
        // given => teniendo un usuario autenticado y los datos del cliente y el viaje
        $this->setActingAs();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $travel = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);
        $client = factory(Client::class)->create();
        $travel->clients()->attach($client->id);

        // when => cuando se hace get request a /api/travels/{travel}/clients
        $response = $this->json('get', '/api/travels/' . $travel->id . '/clients/' . $client->id);

        // then => entonces de retorna un status code 200 y la lista de clientes
        $response->assertStatus(200)
            ->assertJson(
            [
                'data' => [
                    
                    'id' => $travel->id,
                    'code' => $travel->code,
                    'price' => $travel->price,
                    'places' => $travel->places,
                    'currency' => $travel->currency,
                   
                    'clients' => [
                        [
                            'id' => $client->id
                        ]
                    ]
                    
                ] 
            ]
        );
    }


    public function url() {
        return '';
    }
}
