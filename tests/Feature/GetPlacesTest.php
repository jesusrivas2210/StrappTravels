<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Place;

class GetPlacesTest extends TestCase
{
    protected $places;

    public function setUp(){
        parent::setUp();
        $this->places = factory(Place::class)->create();
    }

    /**
     * @test
     */
    public function an_authenticated_user_can_get_all_places()
    {
        // given => teniendo un usuario autenticado y varios lugares en la base de datos
        $this->setActingAs();

        // when => cuando se hace get request a /api/places
        $response = $this->json('get', $this->url());

        // then => entonces de retorna un status code 200 y la lista de lugares
        $response->assertStatus(200)->assertJson(
            [
                'data' => [
                    [
                        'id' => $this->places->id,
                        'place' => $this->places->place,                        
                        'created_at' => $this->places->created_at,
                        'updated_at' => $this->places->updated_at,
                    ]
                ] 
            ]
        );
    }

    /**
     * @test
     */
    public function an_user_cannot_get_places()
    {
        // given => teniendo varios lugares en la base de datos y un usuario no autenticado
        // when => cuando se hace get request a /api/places
        $response = $this->json('get', $this->url());

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_can_get_place_by_id()
    {
        // given => teniendo un usuario autenticado y varios lugares en la base de datos
        $this->setActingAs();

        // when => cuando se hace get request a /api/places/{place}
        $response = $this->json('get', $this->url() . '/' . $this->places->id);

        // then => entonces de retorna un status code 200 y el lugar del id
        $response->assertStatus(200)->assertJson(
            [
                'data' => [
                    'id' => $this->places->id,
                    'place' => $this->places->place,                        
                    'created_at' => $this->places->created_at,
                    'updated_at' => $this->places->updated_at,
                    
                ] 
            ]
        );  
    
    }

    /**
     * @test
     */
    public function an_user_cannot_get_place_by_id()
    {
        // given => teniendo varios lugares en la base de datos y un usuario no autenticado

        // when => cuando se hace get request a /api/places/{place}
        $response = $this->json('get', $this->url() . '/' . $this->places->id);

         // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_get_place_by_id_that_not_exists()
    {
        // given => teniendo un usuario autenticado y varios lugares en la base de datos
        $this->setActingAs();

        // when => cuando se hace get request a /api/places/{place}
        $response = $this->json('get', $this->url() . '/1000');

        // then => entonces de retorna un status code 404 not found
        $response->assertStatus(404);   
    
    }

    public function url() {
        return 'api/places';
    }
}
