<?php

namespace Tests\Feature;

use Tests\TestCase;

class CreateClientTest extends TestCase
{
    /**
     * @test
     */
    public function an_authenticated_user_can_create_clients()
    {
        // given => teniendo un usuario autenticado y los datos del cliente
        $this->setActingAs();
        $payload = [
        	'first_name' => 'Jesus',
        	'last_name' => 'Rivas',
        	'id_number' => '15198109',
        	'address' => 'Urb. Union calle principal, Caracas',
        	'phone' => '1234567890'
        ];

        // when => cuando se hace post request a /api/clients
        $response = $this->json('POST', $this->url(), $payload);

        // then => entonces de retorna un status code 201 y el nuevo cliente creado.
        //         Y se ve el nuevo registro en la base de datos
        $response->assertStatus(201)
        		->assertJson(
        			[
        				'data' => $payload
        			]	
        		);
        $table = 'clients';
        $this->assertDatabaseHas($table, $payload);
    }

    /**
     * @test
     */
    public function an_user_cannot_create_clients()
    {
    	// given => teniendo un usuario NO autenticado y los datos del cliente
        $payload = [
        	'first_name' => 'Jesus',
        	'last_name' => 'Rivas',
        	'id_number' => '15198109',
        	'address' => 'Urb. Union calle principal, Caracas',
        	'phone' => '1234567890'
        ];

        // when => cuando se hace post request a /api/clients
        $response = $this->json('POST', $this->url(), $payload);

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_clients_with_invalid_first_name()
    {
    	// given => teniendo un usuario autenticado y el nombre del cliente es no valido
        $this->setActingAs();
        $field = 'first_name';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 100;
        $this->inputValidation($value, $field);

        $value = str_random(101);
        $this->inputValidation($value, $field);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_clients_with_invalid_last_name()
    {
        // given => teniendo un usuario autenticado y el apellido del cliente no valido
        $this->setActingAs();
        $field = 'last_name';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 100;
        $this->inputValidation($value, $field);

        $value = str_random(101);
        $this->inputValidation($value, $field);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_clients_with_invalid_id_number()
    {
        // given => teniendo un usuario autenticado y la cedula del cliente no valida
        $this->setActingAs();
        $field = 'id_number';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 'cedula';
        $this->inputValidation($value, $field);

        $value = '12345';
        $this->inputValidation($value, $field);

        $value = '12345678901';
        $this->inputValidation($value, $field);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_clients_with_same_id_number()
    {
        // given => teniendo un usuario autenticado y los datos del cliente
        $this->setActingAs();
        $payload = [
            'first_name' => 'Jesus',
            'last_name' => 'Rivas',
            'id_number' => '15198109',
            'address' => 'Urb. Union calle principal, Caracas',
            'phone' => '1234567890'
        ];

        // when => cuando se hacen dos post request a /api/clients
        $this->json('POST', $this->url(), $payload);
        $response = $this->json('POST', $this->url(), $payload);

        // then => entonces de retorna un status code 422 Unprocessable Entity
        $response->assertStatus(422)
                ->assertJsonValidationErrors('id_number');
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_clients_with_invalid_address()
    {
        $this->setActingAs();
        $field = 'address';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 100;
        $this->inputValidation($value, $field);

        $value = str_random(151);
        $this->inputValidation($value, $field);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_create_clients_with_invalid_phone()
    {
        // given => teniendo un usuario autenticado y el telefono del cliente no valido
        $this->setActingAs();
        $field = 'phone';
        $value = '';
        $this->inputValidation($value, $field);

        $value = 'telefono';
        $this->inputValidation($value, $field);

        $value = '123456789';
        $this->inputValidation($value, $field);

        $value = '12345678901';
        $this->inputValidation($value, $field);
    }

    public function url() {
        return 'api/clients';
    }
}
