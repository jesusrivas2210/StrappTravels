<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Client;

class UpdateClientTest extends TestCase
{
    protected $client;

    public function setUp(){
        parent::setUp();
        $this->client = factory(Client::class)->create();
    }
    
    /**
     * @test
     */
    public function an_authenticated_user_can_update_clients()
    {
    	// given => teniendo un usuario autenticado, un cliente en la base de datos y los
    	// datos para actualizar al cliente
        $this->setActingAs();
        $payload = [
        	'first_name' => 'Jesus',
        	'last_name' => 'Rivas',
        	'id_number' => '15198109',
        	'address' => 'Urb. Union calle principal, Caracas',
        	'phone' => '1234567890'
        ];

        // when => cuando se hace patch request a /api/clients/{client}
        $response = $this->json('PATCH', $this->url(), $payload);

        // then => entonces de retorna un status code 201 y el nuevo cliente creado.
        //         Y se ve el nuevo registro en la base de datos
        $response->assertStatus(200)
                ->assertJson(
                    [
                        'data' => $payload
                    ]   
                );
        $table = 'clients';
        $this->assertDatabaseHas($table, $payload);
    }

    /**
     * @test
     */
    public function an_user_cannot_update_clients()
    {
        // given => teniendo un usuario NO autenticadoun, un cliente en la base de datos y los
        // datos para actualizar al cliente
        $payload = [
            'first_name' => 'Jesus',
            'last_name' => 'Rivas',
            'id_number' => '15198109',
            'address' => 'Urb. Union calle principal, Caracas',
            'phone' => '1234567890'
        ];

        // when => cuando se hace post request a /api/clients
        $response = $this->json('PATCH', $this->url(), $payload);

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_clients_with_invalid_first_name()
    {
        $this->withExceptionHandling();
        // given => teniendo un usuario autenticado y el nombre del cliente es no valido
        $this->setActingAs();
        $field = 'first_name';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 100;
        $this->inputValidation($value, $field, 'PATCH');

        $value = str_random(101);
        $this->inputValidation($value, $field, 'PATCH');
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_clients_with_invalid_last_name()
    {
        // given => teniendo un usuario autenticado y el apellido del cliente no valido
        $this->setActingAs();
        $field = 'last_name';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 100;
        $this->inputValidation($value, $field, 'PATCH');

        $value = str_random(101);
        $this->inputValidation($value, $field, 'PATCH');
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_clients_with_invalid_id_number()
    {
        // given => teniendo un usuario autenticado y la cedula del cliente no valida
        $this->setActingAs();
        $field = 'id_number';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 'cedula';
        $this->inputValidation($value, $field, 'PATCH');

        $value = '12345';
        $this->inputValidation($value, $field, 'PATCH');

        $value = '12345678901';
        $this->inputValidation($value, $field, 'PATCH');
        
        $this->inputValidation($this->client->id_number, $field, 'PATCH');
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_clients_with_same_id_number()
    {
        $this->setActingAs();
        $field = 'id_number';
        $otherClient = factory(Client::class)->create();
        $this->inputValidation($otherClient->id_number, $field, 'PATCH');
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_clients_with_invalid_address()
    {
        $this->setActingAs();
        $field = 'address';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 100;
        $this->inputValidation($value, $field, 'PATCH');

        $value = str_random(151);
        $this->inputValidation($value, $field, 'PATCH');
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_clients_with_invalid_phone()
    {
        // given => teniendo un usuario autenticado y el telefono del cliente no valido
        $this->setActingAs();
        $field = 'phone';
        $value = '';
        $this->inputValidation($value, $field, 'PATCH');

        $value = 'telefono';
        $this->inputValidation($value, $field, 'PATCH');

        $value = '123456789';
        $this->inputValidation($value, $field, 'PATCH');

        $value = '12345678901';
        $this->inputValidation($value, $field, 'PATCH');
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_update_clients_that_not_exists()
    {
        // given => teniendo un usuario autenticado y un cliente en la base de datos
        $this->setActingAs();
        $payload = [
            'phone' => '1234567890'
        ];

        // when => cuando se hace un patch request a la url con un cliente inexistente
        $response = $this->json('PATCH', 'api/clients/1000', $payload);

        // then => entonces de retorna un status code 404 NOT FOUND
        $response->assertStatus(404);
    }

    public function url() {
        return 'api/clients/' . $this->client->id;
    }
}
