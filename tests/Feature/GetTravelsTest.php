<?php

namespace Tests\Feature;

use Tests\TestCase;
use StrappTravels\Models\Place;
use StrappTravels\Models\Travel;

class GetTravelsTest extends TestCase
{
    protected $travels;

    public function setUp(){
        parent::setUp();
        $origen = factory(Place::class)->create();
        $destiny = factory(Place::class)->create();
        $this->travels = factory(Travel::class)->create([
            'origen_id' => $origen->id,
            'destiny_id' => $destiny->id
        ]);
    }

    /**
     * @test
     */
    public function an_authenticated_user_can_get_all_travels()
    {
        // given => teniendo un usuario autenticado y varios viajes en la base de datos
        $this->setActingAs();

        // when => cuando se hace get request a /api/travels
        $response = $this->json('get', $this->url());

        // then => entonces de retorna un status code 200 y la lista de viajes
        $response->assertStatus(200)->assertJson(
            [
                'data' => [
                    [
                        'id' => $this->travels->id,
                        'code' => $this->travels->code,
                        'price' => $this->travels->price,
                        'places' => $this->travels->places,
                        'currency' => $this->travels->currency,
                        'created_at' => $this->travels->created_at,
                        'updated_at' => $this->travels->updated_at,
                    ]
                ] 
            ]
        );
    }

    /**
     * @test
     */
    public function an_user_cannot_get_travels()
    {
        // given => teniendo varios viajes en la base de datos y un usuario no autenticado
        // when => cuando se hace get request a /api/travels
        $response = $this->json('get', $this->url());

        // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_can_get_travel_by_id()
    {
        // given => teniendo un usuario autenticado y varios viajes en la base de datos
        $this->setActingAs();

        // when => cuando se hace get request a /api/travels/{travel}
        $response = $this->json('get', $this->url() . '/' . $this->travels->id);

        // then => entonces de retorna un status code 200 y el viaje del id
        $response->assertStatus(200)->assertJson(
            [
                'data' => [
                    
                    'id' => $this->travels->id,
                        'code' => $this->travels->code,
                        'price' => $this->travels->price,
                        'places' => $this->travels->places,
                        'currency' => $this->travels->currency,
                        'created_at' => $this->travels->created_at,
                        'updated_at' => $this->travels->updated_at,
                    
                ] 
            ]
        );  
    
    }

    /**
     * @test
     */
    public function an_user_cannot_get_travel_by_id()
    {
        // given => teniendo varios viajes en la base de datos y un usuario no autenticado

        // when => cuando se hace get request a /api/travels/{travel}
        $response = $this->json('get', $this->url() . '/' . $this->travels->id);

         // then => entonces de retorna un status code 401 no autorizado
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function an_authenticated_user_cannot_get_travel_by_id_that_not_exists()
    {
        // given => teniendo un usuario autenticado y varios viajes en la base de datos
        $this->setActingAs();

        // when => cuando se hace get request a /api/travels/{travel}
        $response = $this->json('get', $this->url() . '/1000');

        // then => entonces de retorna un status code 404 not found
        $response->assertStatus(404);   
    
    }


    public function url() {
        return 'api/travels';
    }
}
