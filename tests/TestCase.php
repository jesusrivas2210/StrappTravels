<?php

namespace Tests;

use StrappTravels\User;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    public function setActingAs() {
        $user = factory(User::class)->create();
        Passport::actingAs($user);
    }

    protected function inputValidation($value, $field, $method = 'POST') {
        $payload = [
            $field => $value
        ];

        // when => cuando se hace un post o patch request a la url con data incorrecta
        $response = $this->json($method, $this->url(), $payload);

        // then => entonces de retorna un status code 422 Unprocessable Entity
        $response->assertStatus(422)
                ->assertJsonValidationErrors($field);
    }

    public abstract function url();
}
