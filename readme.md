## About StrappTravels - Prueba de conocimiento

Una agencia de viajes desea automatizar toda la gestión de los clientes que acuden a su
oficina y los viajes que estos realizan. Esta aplicacion hecha con Laravel 5.6 proporciona una API para dicha gestion.

## Pasos para la instalación

1. Crear directorio para el proyecto, por ejemplo:

$ mkdir strapp-travels


2. Ir al directorio creado anteriormente:

$ cd strapp-travles


3. Clonar el repositorio de Gitlab: 

$ git clone https://gitlab.com/jesusrivas2210/StrappTravels.git 


4. Renombrar el archivo .env.example:

$ cp .env.example .env


5. Configurar la conexión a la base de datos: 

	Abrir el archivo .env con un editor de texto y cambiar

	DB_CONNECTION=mysql

	por 

	DB_CONNECTION=sqlite

	Para usar sqlite, o consultar la documentación oficial 
	Laravel https://laravel.com/docs/5.6/database#configuration para mas información


6. Ejecutar la migraciones:

$ php artisan migrate


7. Crear el cliente para acceder a la API mediante Access Token (Laravel Passport)

$ php artisan passport:client --password

Cambiar el nombre o dejar el que sugueire la consola de comandos

Copiar el client_id y el client_secret para usarlos en las pruebas de la API.


8. Crear un usuario de prueba:

$ php artisan db:seed

Se creará un usuario con email travel@correo.com y password secret, necesario para la autenticación junto con el client_id y el client_secret del paso anterior.


9. Ejecutar los test con phpunit (Opcional):

$ vendor/bin/phpinit


10. Ejecutar el servidor para realizar las pruebas:

$ php artisan serve



## Uso de la API

La API requiere un token de acceso para su uso, se puede obtener de la siguiente forma:

1. Hacer un post request a la ruta localhost:8000/oauth/token con los siguientes campos en el body de la petición:

	{
		"grant_type": "password",
		"client_id": "el client id del paso 7 de la instalacion",
		"client_secret": "el client secret del paso 7 de la instalacion",
		"username": "travel@correo.com",
		"password": "secret",
		"scope": ""
	}

	
	Se genera una respuesta como la siguiente:

	{
    "token_type": "Bearer",
    "expires_in": 31536000,
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjdlYTI1Y2NmZjYxNjdlMmEwMWY2NGY4ZmM0OTlkMjNmYTkxZmU0NDk5NGRlMWU0ZTYzMWFmMTYzYTk5OWIzYjc5NTAxMTAxODgwOTBmNjFmIn0.eyJhdWQiOiIxIiwianRpIjoiN2VhMjVjY2ZmNjE2N2UyYTAxZjY0ZjhmYzQ5OWQyM2ZhOTFmZTQ0OTk0ZGUxZTRlNjMxYWYxNjNhOTk5YjNiNzk1MDExMDE4ODA5MGY2MWYiLCJpYXQiOjE1MzI4MDIwNjAsIm5iZiI6MTUzMjgwMjA2MCwiZXhwIjoxNTY0MzM4MDYwLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.ZoVsAfhIedhyx8mZk_PxsSfG6COwQQctxvABgCLVHmoetc4wzU2CkEjc7__-RoxiNvKxC0lieViV2s8Z71PA_yXpFmG-5qOgHDRxHjMoOM_zuoQb5orKu_NkNBpZypML9GJiKQ_CIxydZQX6Xm85gqfjFBgdpiMIZr2VWz0jZIFMmneJBW5d9ntQS8lRfyUACvaSH8acJeNGw5S5W4Mq661z9RBLNv23gZQQw0dO7CDW03no3_w0Z-CwvSJaxgbaco6fxRy5qaYgo-Z367ofQ6evZs2dkZCrGwK4FAOh6yK69nw89RtbGzMa4tVcXQXR1uRxUs5f6vimivB0F1Y4QGRMQWRafX1TDpfBLi29-kXsimX_AMNlV5W8hj8K7vvPSF8ZLjUjpKKrN8yOosvAhyTX0mxpq1mavJjRaBnIbJ7_M_GGpl9I1uIhHNIfuvUzGRwh3bY-NN_8ABhbm88HKuFoWhtCi57WaiZV8IEcKp4Ci0gE1iTVSQv-00pK7uExNR49YdY2ZjoRMkM0dNcGA1EYvIrPUlGRWv-sMGOWmepcZCu0OCCZL6PHrO_C7U47p7agfFYsfDAwvTE-jOshLyorBuK1AON9af5NC6jGFMgZ3CxNEPidTT6KDMWMPDiURk-oPTPy_ist6fQRQKm3JkSpiflX3KQD_v_JoKLkdS4",
    "refresh_token": "def5020094f393a22faa21c5f29046047c3d35b0d57d7e0b51b07f65fda2e14cb28b58e8d3b289e81d4a9d3edcc86d47f3e697f3e054c2c85bf1b3812c151b42103f8bc1dea9c993828edcf4959b432377faaba72eaa24de6cfa64e872502e0bfd3aebc6df329561f2291c3f80ed0e5a5acd17153c08954a98d7d543debe8992db26ca447b81cca25380bb5b46494b9316db55540d0bbeb24eb043bd1f149b39dfe8fd69b692f9e1edf5936a5cd82b4f12e6cdb6bf66e0b014f493f7ed2de65a051b0c3c91d4178ced5b6cbc9d035f558d42214b2e995ad73d1a8ab48de414ca79ea6531e838456e8b18a0743cd10daba215c231d7e61f7f750199a67ddc24c1e1eef3c67812bf0cf76b07701f4dfc6a1a6d086b730e4a265431eadd5515f7053f632cc77dc2437b8d4716ecc3c48f8f11a9e6844669f89510dae47c1c2ee29522522449326ce4860bec0fc149a6cb6c4ea101b1e1eb82d5be5badb668f2a9d9e2"
	}

	copiar solo el access_token, NOTAR el refresh_token: 

	"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjdlYTI1Y2NmZjYxNjdlMmEwMWY2NGY4ZmM0OTlkMjNmYTkxZmU0NDk5NGRlMWU0ZTYzMWFmMTYzYTk5OWIzYjc5NTAxMTAxODgwOTBmNjFmIn0.eyJhdWQiOiIxIiwianRpIjoiN2VhMjVjY2ZmNjE2N2UyYTAxZjY0ZjhmYzQ5OWQyM2ZhOTFmZTQ0OTk0ZGUxZTRlNjMxYWYxNjNhOTk5YjNiNzk1MDExMDE4ODA5MGY2MWYiLCJpYXQiOjE1MzI4MDIwNjAsIm5iZiI6MTUzMjgwMjA2MCwiZXhwIjoxNTY0MzM4MDYwLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.ZoVsAfhIedhyx8mZk_PxsSfG6COwQQctxvABgCLVHmoetc4wzU2CkEjc7__-RoxiNvKxC0lieViV2s8Z71PA_yXpFmG-5qOgHDRxHjMoOM_zuoQb5orKu_NkNBpZypML9GJiKQ_CIxydZQX6Xm85gqfjFBgdpiMIZr2VWz0jZIFMmneJBW5d9ntQS8lRfyUACvaSH8acJeNGw5S5W4Mq661z9RBLNv23gZQQw0dO7CDW03no3_w0Z-CwvSJaxgbaco6fxRy5qaYgo-Z367ofQ6evZs2dkZCrGwK4FAOh6yK69nw89RtbGzMa4tVcXQXR1uRxUs5f6vimivB0F1Y4QGRMQWRafX1TDpfBLi29-kXsimX_AMNlV5W8hj8K7vvPSF8ZLjUjpKKrN8yOosvAhyTX0mxpq1mavJjRaBnIbJ7_M_GGpl9I1uIhHNIfuvUzGRwh3bY-NN_8ABhbm88HKuFoWhtCi57WaiZV8IEcKp4Ci0gE1iTVSQv-00pK7uExNR49YdY2ZjoRMkM0dNcGA1EYvIrPUlGRWv-sMGOWmepcZCu0OCCZL6PHrO_C7U47p7agfFYsfDAwvTE-jOshLyorBuK1AON9af5NC6jGFMgZ3CxNEPidTT6KDMWMPDiURk-oPTPy_ist6fQRQKm3JkSpiflX3KQD_v_JoKLkdS4"


2. Para probar la API agregar las cabeceras Authorization y Accept con los siguientes valores a las peticiones de prueba a realizar:

 Authorization = Bearer + el access token del paso anterior
 
 Accept = application/json


3. La documentacion completa de los endpoints, son sus metodos, urls y parametros de entrada y respuestas se pueden consultar en http://localhost:8000/api/documentation