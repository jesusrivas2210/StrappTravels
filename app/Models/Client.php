<?php

namespace StrappTravels\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'first_name',
    	'last_name',
    	'id_number',
    	'address',
    	'phone'
    ];

    /**
     * Viajes del cliente
     */
    public function travels()
    {
        return $this->belongsToMany('StrappTravels\Models\Travel');
    }
}
