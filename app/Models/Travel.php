<?php

namespace StrappTravels\Models;

use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'code',
    	'price',
    	'places',
    	'currency',
    	'departure_at',
    	'origen_id',
    	'destiny_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'departure_at'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['origen', 'destiny', 'clients'];

    /**
     * Lugar de origen del viaje
     */
    public function origen()
    {
        return $this->belongsTo('StrappTravels\Models\Place', 'origen_id');
    }

    /**
     * Lugar de destino del viaje
     */
    public function destiny()
    {
        return $this->belongsTo('StrappTravels\Models\Place', 'destiny_id');
    }

    /**
     * Clientes del viaje
     */
    public function clients()
    {
        return $this->belongsToMany('StrappTravels\Models\Client');
    }

    /**
     * Valida si hay lugares o asientos disponibles en el viaje
     */
    public function seatsAvailable()
    {
        return $this->places <= $this->clients()->count();
    }

    /**
     * Scope a query para incluir un cliente especifico del viaje en la consulta.
     */
    public function scopeAndClient($query, $client)
    {
        return $query->with(['clients' => function ($q) use ($client) {
            $q->where('client_id', '=', $client);
        }]);
    }
}
