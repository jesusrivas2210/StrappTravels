<?php

namespace StrappTravels\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    { 
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->wantsJson()) {
            
            if ($exception instanceof ModelNotFoundException) {
                return response()->json([
                    'error' => 'Resource not found.'
                ], 404);
            }

            if ($exception instanceof UnauthorizedException 
                    || $exception instanceof InsufficientPermissionException) {
                return response()->json([
                    'error' => 'Insufficient permission.'
                ], 403);
            }
            
            if ($exception instanceof AuthenticationException) {
                return response()->json([
                    'error' => 'Unauthorized.'
                ], 401);
            }

        }

        return parent::render($request, $exception);
    }
}