<?php

namespace StrappTravels\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class TravelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|numeric|unique:travels',
            'price' => 'required|numeric',
            'places' => 'required|integer',
            'currency' => [
                'required',
                Rule::in(['USD', 'EU']),
            ],
            'departure_at' => 'required|date',
            'origen_id' => 'required|integer',
            'destiny_id' => 'required|integer|different:origen_id',
        ];
    }
}
