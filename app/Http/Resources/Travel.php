<?php

namespace StrappTravels\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Travel extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'price' => $this->price,
            'places' => $this->places,
            'currency' => $this->currency,
            'departure_at' => (string)$this->departure_at,
            'created_at' => (string)$this->created_at,
            'updated_at' => (string)$this->updated_at,
            'origen' => $this->origen,
            'destiny' => $this->destiny,
            'clients' => Client::collection($this->whenLoaded('clients'))
        ];
    }
}
