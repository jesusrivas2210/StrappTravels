<?php

namespace StrappTravels\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Client extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'id_number' => $this->id_number,
            'address' => $this->address,
            'phone' => $this->phone,
            'created_at' => (string)$this->created_at,
            'updated_at' => (string)$this->updated_at,   
            'travels' => Travel::collection($this->whenLoaded('travels')),         
        ];
    }
}
