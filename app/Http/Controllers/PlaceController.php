<?php

namespace StrappTravels\Http\Controllers;

use Illuminate\Http\Request;
use StrappTravels\Models\Place;
use StrappTravels\Http\Requests\PlaceRequest;
use StrappTravels\Http\Resources\PlaceCollection;
use StrappTravels\Http\Resources\Place as PlaceResource;

class PlaceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    /**
     * Muestra una lista de lugares
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $places = Place::all();

        return new PlaceCollection($places);
    }

    /**
     * Almacena un lugar
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlaceRequest $request)
    {
        $place = Place::create($request->all());

        return new PlaceResource($place);
    }

    /**
     * Muestra un lugar espscifico.
     *
     * @param  \StrappTravels\Models\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function show(Place $place)
    {
        return new PlaceResource($place);
    }

    /**
     * Actualiza un lugar especifico
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \StrappTravels\Models\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function update(PlaceRequest $request, Place $place)
    {
        $place->update($request->all());

        return new PlaceResource($place);
    }

    /**
     * Elimina un lugar especifico
     *
     * @param  \StrappTravels\Models\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function destroy(Place $place)
    {
        $place->delete();

        return response()->json(['deleted' => true]);
    }
}
