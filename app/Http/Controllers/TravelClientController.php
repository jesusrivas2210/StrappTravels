<?php

namespace StrappTravels\Http\Controllers;

use Illuminate\Http\Request;
use StrappTravels\Models\Client;
use StrappTravels\Models\Travel;
use StrappTravels\Http\Resources\TravelCollection;
use StrappTravels\Http\Requests\TravelClientRequest;
use StrappTravels\Http\Resources\Travel as TravelResource;
use StrappTravels\Http\Resources\Client as ClientResource;

class TravelClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra un viaje con sus clientes agregados.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Travel $travel)
    {
        return new TravelResource($travel);
    }

    /**
     * Muestra un cliente con sus viajes
     *
     * @return \Illuminate\Http\Response
     */
    public function clientTravels($client) 
    {
        $clientTravels = Client::with('travels')->find($client);

        return new ClientResource($clientTravels);
    }

    /**
     * Agrega un cliente a un viaje
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TravelClientRequest $request, Travel $travel)
    {
        $clientId = $request->client_id;

        // valida que el cliente exista en la base de datos
        $client = Client::find($clientId);
        if (!$client) {
            abort(422, 'Error en los datos de entrada. client_id no existe.');
        }

        // valida que existan plazas disponibles
        if ($travel->seatsAvailable()) {
            abort(400, 'No se pueden agregar mas clientes a este viaje.');
        }

        // valida que el cliente no este ya en el viaje
        $client = $travel->clients()->where('client_id', $clientId)->first();
        if ($client) {
            abort(400, 'El cliente ya esta en el viaje.');
        }

        // agrega el cliente al viaje
        $travel->clients()->attach($clientId);

        return response()->json(['attached' => true], 201);
    }

    /**
     * Muestra un viaje con un cliente especifico
     *
     * @return \Illuminate\Http\Response
     */
    public function show($travel, $client) 
    {
        $travel = Travel::find($travel);
        if ($travel == null) {
            abort(404, "Viaje no existe");
        }
        $travel = $travel->andClient($client)->first();
        
        return new TravelResource($travel);
    }

    /**
     * Quita un cliente de un viaje especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Travel $travel, $client)
    {
        $travel->clients()->detach($client);

        return response()->json(['deleted' => true]);
    }
}
