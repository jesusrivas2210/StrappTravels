<?php

namespace StrappTravels\Http\Controllers;

use Illuminate\Http\Request;
use StrappTravels\Models\Travel;
use StrappTravels\Http\Requests\TravelRequest;
use StrappTravels\Http\Resources\TravelCollection;
use StrappTravels\Http\Resources\Travel as TravelResource;

class TravelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $travels = Travel::all();

        return new TravelCollection($travels);
    }

    /**
     * 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TravelRequest $request)
    {
        $travel = Travel::create($request->all());

        return new TravelResource($travel);
    }

    /**
     * Display the specified resource.
     *
     * @param  \StrappTravels\Models\Travel  $travel
     * @return \Illuminate\Http\Response
     */
    public function show(Travel $travel)
    {
        return new TravelResource($travel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \StrappTravels\Models\Travel  $travel
     * @return \Illuminate\Http\Response
     */
    public function update(TravelRequest $request, Travel $travel)
    {
        $travel->update($request->all());

        return new TravelResource($travel);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \StrappTravels\Models\Travel  $travel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Travel $travel)
    {
        $travel->delete();

        return response()->json(['deleted' => true]);
    }
}
