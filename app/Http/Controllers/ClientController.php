<?php

namespace StrappTravels\Http\Controllers;

use Illuminate\Http\Request;
use StrappTravels\Models\Client;
use StrappTravels\Http\Requests\ClientRequest;
use StrappTravels\Http\Resources\ClientCollection;
use StrappTravels\Http\Resources\Client as ClientResource;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    /**
     * Muestra un listado de clientes
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clients = Client::get();

        return new ClientCollection($clients);
    }

    /**
     * Almacena un cliente
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {    
        $client = Client::create($request->all());

        return new ClientResource($client);
    }

    /**
     * Muestra un cliente especifico
     *
     * @param  \StrappTravels\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return new ClientResource($client);
    }

    /**
     * Actualiza un cliente especifico
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \StrappTravels\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(ClientRequest $request, Client $client)
    {
        $client->update($request->all());

        return new ClientResource($client);
    }

    /**
     * Elimina un cliente especifico
     *
     * @param  \StrappTravels\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return response()->json(['deleted' => true]);
    }

}
