<?php

/**
 * @SWG\Swagger(
 *   basePath="/",
 *   @SWG\Info(
 *     title="StrappTravels API",
 *     version="1.0.0"
 *   )
 * )
 */

/**
 * @SWG\Post(
 * path="/oauth/token",
 *   summary="Peticion de access token",
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     required=true,
 *     description="Credenciales para solicitar el token de acceso",
 *     @SWG\Schema(
 *         @SWG\Property(property="grant_type", type="string", example="password"),
 *         @SWG\Property(property="client_id", type="string", example="1"),
 *         @SWG\Property(property="client_secret", type="string", example="I3ynp7gY1rncOIBImy3ZhFOEeh7uswtoo9QVJwe2"),
 *         @SWG\Property(property="username", type="string", example="travel@correo.com"),
 *         @SWG\Property(property="password", type="string", example="secret"),
 *         @SWG\Property(property="scope", type="string", example="*")
 *     )
 *   ),
 * 
 *   @SWG\Response(response=200, description="successful operation"),
 *   @SWG\Response(response=422, description="input errors"),
 *   @SWG\Response(response=401, description="Unauthorized")
 * )
 */ 

/**
 * @SWG\Post(
 * path="/api/clients",
 *   summary="Almacena un nuevo cliente en la base de datos",
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     required=true,
 *     description="Cliente a almacenar en la base de datos",
 *     @SWG\Schema(
 *         @SWG\Property(property="first_name", type="string", example="Jesus"),
 *         @SWG\Property(property="last_name", type="string", example="Rivas"),
 *         @SWG\Property(property="id_number", type="integer", example=123456789),
 *         @SWG\Property(property="address", type="string", example="Calle principal"),
 *         @SWG\Property(property="phone", type="integer", example="1234567890")
 *     )
 *   ),
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Response(response=201, description="successful operation"),
 *   @SWG\Response(response=422, description="input errors"),
 *   @SWG\Response(response=401, description="Unauthorized")
 * )
 */ 

/**
 * @SWG\Patch(
 * path="/api/clients/{client}",
 *   summary="Actualiza cliente en la base de datos",
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     required=true,
 *     description="Datos a actualizar en la base de datos",
 *     @SWG\Schema(
 *         @SWG\Property(property="first_name", type="string", example="Jesus"),
 *         @SWG\Property(property="last_name", type="string", example="Rivas"),
 *         @SWG\Property(property="id_number", type="integer", example=123456789),
 *         @SWG\Property(property="address", type="string", example="Calle principal"),
 *         @SWG\Property(property="phone", type="integer", example="1234567890")
 *     )
 *   ),
 *   @SWG\Parameter(
 *          name="client",
 *          description="Id del cliente",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *  @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Response(response=200, description="successful operation"),
 *   @SWG\Response(response=422, description="input errors"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *   @SWG\Response(response=401, description="Unauthorized")
 * )
 */ 

/**
 * @SWG\Post(
 * path="/api/places",
 *   summary="Almacena un nuevo lugar en la base de datos",
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     required=true,
 *     description="Lugar a almacenar en la base de datos.",
 *     @SWG\Schema(
 *         @SWG\Property(property="place", type="string", example="Caracas, Venezuela")
 *     )
 *   ),
 *   @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Response(response=201, description="successful operation"),
 *   @SWG\Response(response=422, description="input errors"),
 *   @SWG\Response(response=401, description="Unauthorized")
 * )
 */

/**
 * @SWG\Patch(
 * path="/api/places/{place}",
 *   summary="Actualiza un lugar en la base de datos",
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     required=true,
 *     description="Datos a actualizar en la base de datos.",
 *     @SWG\Schema(
 *         @SWG\Property(property="place", type="string", example="Caracas, Venezuela")
 *     )
 *   ),
 *   @SWG\Parameter(
 *          name="place",
 *          description="Id del lugar",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *   @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Response(response=200, description="successful operation"),
 *   @SWG\Response(response=422, description="input errors"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *   @SWG\Response(response=401, description="Unauthorized")
 * )
 */

/**
 * @SWG\Post(
 * path="/api/travels/{travel}/clients",
 *   summary="Agrega un cliente a un viaje",
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     required=true,
 *     description="Id del cliente a agregar al viaje.",
 *     @SWG\Schema(
 *         @SWG\Property(property="client_id", type="integer", example="1")
 *     )
 *   ),
 *   @SWG\Parameter(
 *          name="travel",
 *          description="Id del viaje",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *   @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Response(response=201, description="successful operation"),
 *   @SWG\Response(response=422, description="input errors"),
 *   @SWG\Response(response=401, description="Unauthorized"),
 *   @SWG\Response(response=400, description="incomplete operation")
 * )
 */

/**
 * @SWG\Post(
 * path="/api/travels",
 *   summary="Almacena un nuevo viaje en la base de datos",
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     required=true,
 *     description="Viaje a almacenar en la base de datos",
 *     @SWG\Schema(
 *         @SWG\Property(property="code", type="integer", example="123456789"),
 *         @SWG\Property(property="price", type="float", example="1000"),
 *         @SWG\Property(property="places", type="integer", example=10),
 *         @SWG\Property(property="currency", type="string", example="USD"),
 *         @SWG\Property(property="departure_at", type="timestamp", example="2018-07-08 12:00:00"),
 *         @SWG\Property(property="origen_id", type="integer", example="1"),
 *         @SWG\Property(property="destiny_id", type="integer", example="2")
 *     )
 *   ),
 *   @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Response(response=201, description="successful operation"),
 *   @SWG\Response(response=422, description="input errors"),
 *   @SWG\Response(response=401, description="Unauthorized")
 * )
 */

/**
 * @SWG\Patch(
 * path="/api/travels/{travel}",
 *   summary="Actualiza un viaje en la base de datos",
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     required=true,
 *     description="Datos a actualizar en la base de datos",
 *     @SWG\Schema(
 *         @SWG\Property(property="code", type="integer", example="123456789"),
 *         @SWG\Property(property="price", type="float", example="1000"),
 *         @SWG\Property(property="places", type="integer", example=10),
 *         @SWG\Property(property="currency", type="string", example="USD"),
 *         @SWG\Property(property="departure_at", type="timestamp", example="2018-07-08 12:00:00"),
 *         @SWG\Property(property="origen_id", type="integer", example="1"),
 *         @SWG\Property(property="destiny_id", type="integer", example="2")
 *     )
 *   ),
 *   @SWG\Parameter(
 *          name="travel",
 *          description="Id del viaje",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *   @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Response(response=200, description="successful operation"),
 *   @SWG\Response(response=422, description="input errors"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *   @SWG\Response(response=401, description="Unauthorized")
 * )
 */

/**
 * @SWG\Get(
 *      path="/api/clients",
 *      
 *      summary="Obtener lista de clientes",
 *      description="Retorna lista de clientes",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=401, description="Unauthorized"),
 *       @SWG\Response(response=404, description="Not Found"),
 *     )
 *
 * Retorna lista de clientes
 */

/**
 * @SWG\Get(
 *      path="/api/clients/{client}",
 *      
 *      summary="Obtener cliente con el id {client}",
 *      description="Retorna cliente con el id {client}",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 * @SWG\Parameter(
 *          name="client",
 *          description="Id del cliente",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"),
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Retorna lista de clientes
 */

/**
 * @SWG\Get(
 *      path="/api/travels",
 *      
 *      summary="Obtener lista de viajes",
 *      description="Retorna lista de viajes",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"),
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Retorna lista de viajes
 */

/**
 * @SWG\Get(
 *      path="/api/travels/{travel}",
 *      
 *      summary="Obtener viaje con el id {travel}",
 *      description="Retorna viaje con el id {travel}",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *  @SWG\Parameter(
 *          name="travel",
 *          description="Id del viaje",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"),
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Retorna viaje
 */

/**
 * @SWG\Get(
 *      path="/api/travels/{travel}/clients/{client}",
 *      
 *      summary="Obtener viaje con el id {travel} con al cliente de id {client}",
 *      description="Obtener viaje con el id {travel} con al cliente de id {client}",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *  @SWG\Parameter(
 *          name="travel",
 *          description="Id del viaje",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *  @SWG\Parameter(
 *          name="client",
 *          description="Id del cliente",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Retorna viaje con el cliente
 */

/**
 * @SWG\Get(
 *      path="/api/places",
 *      
 *      summary="Obtener lista de lugares",
 *      description="Retorna lista de lugares",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Retorna lista de lugares
 */

/**
 * @SWG\Get(
 *      path="/api/places/{place}",
 *      
 *      summary="Obtener lugar con el id {place}",
 *      description="Obtener lugar con el id {place}",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
   * @SWG\Parameter(
 *          name="place",
 *          description="Id del lugar",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Retorna lugar con el id {place}
 */

/**
 * @SWG\Get(
 *      path="/api/travels/{travel}/clients",
 *      
 *      summary="Obtener el viaje con el id {travel} con sus clientes",
 *      description="Retorna un viaje con sus clientes",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Parameter(
 *          name="travel",
 *          description="Id del viaje",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Retorna un viaje con sus clientes
 */

/**
 * @SWG\Get(
 *      path="/api/clients/{client}/travels",
 *      
 *      summary="Obtener el cliente con el id {client} con sus viajes",
 *      description="Retorna un cliente con sus viajes",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *  @SWG\Parameter(
 *          name="client",
 *          description="Id del cliente",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Retorna un cliente con sus viajes
 */

/**
 * @SWG\Delete(
 *      path="/api/clients/{client}",
 *      
 *      summary="Elimina el cliente con el id {client} ",
 *      description="Elimina el cliente con el id {client}",
 *  @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *  @SWG\Parameter(
 *          name="client",
 *          description="Id del cliente",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Elimina un cliente
 */

/**
 * @SWG\Delete(
 *      path="/api/places/{place}",
 *      
 *      summary="Elimina el lugar con el id {place} ",
 *      description="Elimina el lugar con el id {place}",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 *  @SWG\Parameter(
 *          name="place",
 *          description="Id del lugar",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Elimina un lugar
 */

/**
 * @SWG\Delete(
 *      path="/api/travels/{travel}",
 *      
 *      summary="Elimina el viaje con el id {travel} ",
 *      description="Elimina el viaje con el id {travel}",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 * @SWG\Parameter(
 *          name="travel",
 *          description="Id del viaje",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Elimina un viaje
 */

/**
 * @SWG\Delete(
 *      path="/api/travels/{travel}/clients/{client}",
 *      
 *      summary="Quita el cliente con el id {client} del viaje con el id {travel}",
 *      description="Quita el cliente con el id {client} del viaje con el id {travel}",
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="access token",
 *     required=true,
 *     type="string"
 *   ),
 * @SWG\Parameter(
 *          name="travel",
 *          description="Id del viaje",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *   @SWG\Parameter(
 *          name="client",
 *          description="Id del cliente",
 *          required=true,
 *          type="integer",
 *          in="path"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       @SWG\Response(response=404, description="Not Found"), 
 *       @SWG\Response(response=401, description="Unauthorized")
 *     )
 *
 * Quita un cliente de un viaje
 */